---
layout: post
title: "码云近期更新汇总 —— 仓库分支模型+修改仓库地址"
---

<p>码云平台几乎每天都会做更新，现在将近期我们更新的主要内容汇总一下，包括：</p>

<h3>新建仓库支持自动创建分支模型</h3>

<p>目前提供几种主流的分支模型以及自定义分支模型：</p>

<p><img height="582" src="https://oscimg.oschina.net/oscnet/f1cf6bbbc275fb7c19a99703e69d5398280.jpg" width="1508" /></p>

<h3>码云仓库关注（Watch）支持多种关注策略</h3>

<p>现在码云仓库关注已支持只关注发版(Release)信息，对于关注了大量仓库的用户，可以通过将关注仓库动态从「关注所有动态」改为「仅关注版本发行动态」或「关注但不提醒动态」来减少对自己的信息干扰。相关OpenAPI接口也已同步更新。第三方开发者可以同步支持这一特性。</p>

<p><img alt="" src="https://gitee.com/uploads/images/2019/0409/192750_d40a115a_551147.png" /></p>

<h3>码云支持邮箱隐私保护</h3>

<p>如果用户不希望在码云上公开自己的邮箱信息，可以通过在多邮箱设置中设置「不公开我的邮箱地址」，从而实现在码云网页上和 Gitee WebIDE 提交数据到仓库，使用码云分配的地址作为&nbsp;<code>git config 邮箱</code>。 用户也可以在本地配置对应分配的邮箱地址为&nbsp;<code>git config 邮箱</code>，将代码推送到码云后产生的贡献度依然有效。</p>

<blockquote>
<p>Tips:该邮箱地址仅用于仓库提交和隐私保护，并不能提供任何邮件服务。</p>
</blockquote>

<p><img alt="" src="https://gitee.com/uploads/images/2019/0409/192711_a155c0d8_551147.png" /></p>

<h3>创建Pull Request 接口增加审查、测试人员的指派</h3>

<p>码云的 Pull Request 是附带 Code review 功能的代码合并方式。在网页创建PR是可以指派审查、测试人员，并可以设置审查、测试通过后才可合并PR，可更好地保证您的代码、软件质量。 现已把这个功能开放接口，让码云用户通过api即可指派审查测试人员。详情请点击查看：&nbsp;<a href="https://gitee.com/api/v5/swagger#/postV5ReposOwnerRepoPulls">https://gitee.com/api/v5/swagger#/postV5ReposOwnerRepoPulls</a></p>

<p><img alt="输入图片说明" src="https://gitee.com/uploads/images/2019/0409/163522_d7867102_58426.png" /></p>

<h3>编辑周报添加APIV5</h3>

<p>码云企业版周报是为码云企业版成员提供的便捷填写每周工作报告的功能，码云周报支持用户快捷勾选本周完成的任务、提交的Pull Request、推送的Commits，快速而完整地填写工作报告。 现已把周报功能开放接口，便于码云企业对接到已用的三方平台生成周报。&nbsp;<a href="https://gitee.com/api/v5/swagger#/getV5EnterprisesEnterpriseUsersUsernameWeekReports">https://gitee.com/api/v5/swagger#/getV5EnterprisesEnterpriseUsersUsernameWeekReports</a></p>

<p><img alt="输入图片说明" src="https://gitee.com/uploads/images/2019/0409/164512_dc255dbe_58426.png" /></p>

<h3>企业任务看板增加类型看板</h3>

<p>码云企业版提供了看板模式，在状态看板，及成员看板的基础增加了任务类型看板。</p>

<ul>
	<li>状态看板：传统的看板模式，可方便地拖动任务调整状态。主要用于管理个人任务。</li>
</ul>

<p><img alt="输入图片说明" src="https://gitee.com/uploads/images/2019/0409/170133_8deb90d2_58426.png" /></p>

<ul>
	<li>成员看板：按任务负责人作为板块列出任务，方便任务的指派和改派。主要用于项目经理查看管理任务的分配。</li>
</ul>

<p><img alt="输入图片说明" src="https://gitee.com/uploads/images/2019/0409/170239_b9937c44_58426.png" /></p>

<ul>
	<li>类型看板：按任务类型作为板块列出任务，方便全局查看各个层次的任务。主用于管理人员查看需求、任务、缺陷等分布。</li>
</ul>

<p><img alt="输入图片说明" src="https://gitee.com/uploads/images/2019/0409/170331_3853e4a2_58426.png" /></p>

<h3>&nbsp;</h3>

<h3>&ldquo;我的码云&rdquo;快捷导航增加我的项目列表 码云导航菜单&ldquo;我的码云&rdquo;是进入常用企业、组织、项目、仓库的快捷入口</h3>

<p><img alt="输入图片说明" src="https://gitee.com/uploads/images/2019/0409/170540_7dd0c09f_58426.png" /></p>

<h3>码云企业版任务导入导出功能</h3>

<ul>
	<li>导入任务：码云提供三种格式的导入任务功能，便于企业用户将Team等平台的任务转入。</li>
</ul>

<p><img alt="输入图片说明" src="https://gitee.com/uploads/images/2019/0409/171121_33ea238f_58426.png" /></p>

<ul>
	<li>导出任务：码云任务的导出有两个格式，且用户可以自定义导出内容，便于企业导出任务汇报报告。</li>
</ul>

<p><img alt="输入图片说明" src="https://gitee.com/uploads/images/2019/0409/171239_a1a66afb_58426.png" /></p>

<h3>支持修改仓库路径地址（PATH）</h3>

<p><img alt="输入图片说明" src="https://gitee.com/uploads/images/2019/0409/171641_6b9275f9_58426.png" /></p>

<p>&nbsp;</p>

<p>欢迎大家体验&nbsp; <a href="https://gitee.com">https://gitee.com</a></p>
