---
layout: post
title: "码云 Gitee 微信服务号支持账号登陆通知啦~"
---

<p>对于开发者来说，代码是青春和付出的见证。</p><p>对于IT企业来说，代码是企业的核心和机密。</p><p>对代码托管来说，托管平台账号安全是我们最首要关注的安全问题。</p><p>针对账号安全问题，码云在公众号推出账号登陆行为通知功能。关注公众号「码云Gitee」（微信号mayunOSC），点击菜单「我的」-&gt;「个人主页」，输入你的码云账号密码，即可完成你的码云账号绑定！绑定完成后，账号各类登陆信息，码云让你第一时间掌握，为你的代码保驾护航！</p><p><img src="https://static.oschina.net/uploads/img/201807/03080906_xGmr.jpg" style="width:300;!important"/> &nbsp; &nbsp;<img src="https://static.oschina.net/uploads/img/201807/03080907_ph38.jpg" style="width:300;!important"/></p><p><img src="https://static.oschina.net/uploads/img/201807/03080907_OAmw.jpg" style="width:300;!important"/></p><h2>开启码云登录微信通知，请用微信扫描并关注码云服务号，绑定码云账号：</h2><p><img alt="" height="344" src="https://static.oschina.net/uploads/img/201807/03080907_EEoH.jpg" width="344"/></p>