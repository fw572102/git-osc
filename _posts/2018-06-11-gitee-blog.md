---
layout: post
title: "码云 Pages 全面改进上线 —— 你想要的应该都有"
---

<p>码云 Pages 是一个基于 <a href="https://www.oschina.net/p/kyll">Jekyll</a> 静态网站服务，可以用它来制作个人博客、简历、项目主页、个人主页等等。具体的使用方法请看<a href="http://git.mydoc.io/?t=154714">这里</a>。&nbsp;</p><p>上周我们刚刚对该服务进行全面改进：</p><ul class=" list-paddingleft-2"><li><p>支持发布仓库中某个目录（ 例如你可以将项目的文档目录 doc 发布成静态网页 ）</p></li><li><p>支持自定义域名（如 https://www.i-love-gitee.com ）</p></li><li><p>支持自定义域名 + https</p></li><li><p>仓库推送自动更新 Pages 页面</p></li></ul><p>Pages 服务入口：<strong>项目主页</strong> -&gt; <strong>服务</strong> -&gt; <strong>Gitee Pages</strong></p><p><img src="https://oscimg.oschina.net/oscnet/0496d207b7b43a7890869027ae5e8ec4b66.jpg"/></p><p>前往体验：<a href="https://gitee.com?from=osc-pages-news">https://gitee.com</a>&nbsp;</p><p>如果你是 Java 项目可以使用码云的 JavaDoc 自动生成和托管服务，详情请看<a href="https://www.oschina.net/news/96659/gitee-add-javadoc-generator">这里</a>。</p><p><strong>使用该服务请遵守国内法律。</strong></p>