---
layout: post
title: "码云 Pages 再改进 —— 增加对 Hexo 和 Hugo 支持"
---

<p>继码云 Pages 增加对独立域名和子目录发布以来（<a href="https://www.oschina.net/news/96936/gitee-pages-upgrade" target="_blank">详情</a>），我们继续完善了 Pages 服务，现在除了支持 <a href="https://www.oschina.net/p/kyll">Jekyll</a> 外，我们新增了对 <a href="https://www.oschina.net/p/hexo">Hexo</a> 和 <a href="https://www.oschina.net/p/gohugo">Hugo</a> 两大静态网站生成引擎的支持。</p><p><a href="https://www.oschina.net/p/hexo">Hexo</a> 是一个基于nodejs 的静态博客网站生成器。特点：不可思议的快 ─ 只要一眨眼静态文件即生成完成；支持 Markdown；已移植 Octopress 插件；高扩展性、自订性。</p><p><a href="https://www.oschina.net/p/gohugo">Hugo</a> 是 Go 编写的静态网站生成器，速度快，易用，可配置。Hugo 有一个内容和模板目录，把他们渲染到完全的 HTML 网站。Hugo 依赖于 Markdown 文件，元数据字体 。用户可以从任意的目录中运行 Hugo，支持共享主机和其他系统。</p><p>使用方法：项目主页 -&gt; 服务 -&gt; Gitee Pages</p><p><img alt="" height="254" src="https://oscimg.oschina.net/oscnet/8817740c4ea4ea10761bea7755376437d91.jpg" width="400"/></p><p>即可前往体验： <a href="https://gitee.com?from=gitee-pages-news">https://gitee.com</a></p>